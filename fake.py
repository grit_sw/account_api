import random
from string import ascii_letters
from uuid import uuid4

import arrow
import json


def randomword(length=10):
   return ''.join(random.choice(ascii_letters) for i in range(length))


def random_integer(multipuler=4):
	number_str = ''.join(f"{random.randint(0, 9)}" for _ in range(multipuler))
	return int(number_str)

def random_boolean():
	boolean_tuple = (True, False)
	return random.choice(boolean_tuple)


def random_2dp_number():
	"""
	Creates a random 2 decimal places number
	:return:
	"""
	return round(random.random() * 1000, 2)


# send to new credit endpoint currently a  post request to  /account/credit/
fake_credit_data = {'paid_at': str(arrow.now()),
	'group_id': randomword(16),
	'reference': randomword(5),
	'amount_paid': random_2dp_number(),
	'customer_id': str(uuid4()),
	'tx_performed_by': randomword(),
	'is_working_balance': random_boolean(),
	'paystack_auth': randomword(20),
	'last_four': random_integer(),
	'brand': randomword(6),
	'meter_id': randomword(6),
	'energy_units_purchased': str(random_integer()),
					}

fake_debit_data = {
  "group_id": str(uuid4()),
  "debitor_id": str(uuid4()),
  "amount": random_2dp_number()
}

def gen_fake_date():
	print(f"fake_debit_data .... {json.dumps(fake_debit_data)}\n")
	print(f"fake_credit_data ..... {json.dumps(fake_credit_data)}\n")


if __name__ == "__main__":
	# print(json.dumps(fake_credit_data))
	# print(json.dumps(fake_debit_data))
	gen_fake_date()
