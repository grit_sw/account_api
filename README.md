# account_api

## Seeding data
- run `python fake.py` to get fake data
- Make a post request to the new credit endpoint, which at the time of writing is "/account/credit/" using the fake_credit_data printed as json in the body of the request.
- Make a post request to the new debit endpoint, which at the time of writing is "/account/debit/" using the fake_debit_data printed out as json in the body of the request.
