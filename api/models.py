from api import db
from api.exceptions import ActivationError, NotFoundError, AccountSuspendedError, InsufficientFundsError
from datetime import datetime
import arrow
from logger import logger
from sqlalchemy import desc


class Base(db.Model):
	__abstract__ = True
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
							  onupdate=db.func.current_timestamp())


class Credit(Base):
	"""
		meter_id and energy_units_purchased are optional parameters because
		while they are required for g zero transaction payments,
		they are not required for gtg payments.
	"""
	id = db.Column(db.Integer, unique=True,
				   primary_key=True, autoincrement=True)
	paid_at = db.Column(db.DateTime, nullable=True)
	date_credited = db.Column(db.DateTime, nullable=False)
	reference_id = db.Column(db.String, nullable=True)
	amount_paid = db.Column(db.Integer, nullable=False)
	tx_performed_by = db.Column(db.String, nullable=True)
	meter_id = db.Column(db.String, nullable=True)
	energy_units_purchased = db.Column(db.String, nullable=True)
	# credit is deactivated when the user makes a new payment
	# credit is activated when the user successfully inputs the correct pin.
	# see pin_api for more information
	is_activated = db.Column(db.Boolean, default=False, nullable=False)
	owner_id = db.Column(db.String, db.ForeignKey('account.group_id'))


	def __init__(self, owner, amount_paid, paid_at=None, reference_id=None,  tx_performed_by=None,
		is_working_balance=None, meter_id=None, energy_units_purchased=None):
		self.paid_at = arrow.get(paid_at).to('Africa/Lagos').datetime
		self.date_credited = arrow.now('Africa/Lagos').to('Africa/Lagos').datetime
		self.reference_id = reference_id
		self.amount_paid = amount_paid
		self.tx_performed_by = tx_performed_by
		self.is_activated = False
		if meter_id:
			self.meter_id = meter_id
		if energy_units_purchased:
			self.energy_units_purchased = energy_units_purchased
		self.owner = owner
		self.owner.ledger_balance += amount_paid
		if is_working_balance:
			logger.info("Crediting the available balance")
			self.owner.available_balance += amount_paid

	@staticmethod
	def get_reference(reference_id):
		return Credit.query.filter_by(reference_id=reference_id).first()

	def to_dict(self):
		payload = {
			'amount_paid': self.amount_paid,
			'meter_id': self.meter_id,
			'energy_units_purchased': self.energy_units_purchased,
			'credited_by': self.tx_performed_by,
			'paid_at': str(self.paid_at),
			'date_credited': str(self.date_credited),
			'ledger_balance': self.owner.ledger_balance,
			'available_balance': self.owner.available_balance,
			'pending_amount': self.owner.pending_amount,
			'reference_id': self.reference_id,
		}
		return payload


class Debit(Base):
	id = db.Column(db.Integer, unique=True,
				   primary_key=True, autoincrement=True)
	owner_id = db.Column(db.String, db.ForeignKey('account.group_id'))
	date_debited = db.Column(db.DateTime, nullable=False)
	debitor_id = db.Column(db.String, nullable=True)
	amount = db.Column(db.Integer, nullable=False)

	def __init__(self, debitor_id, amount, owner):
		self.amount = amount
		self.debitor_id = debitor_id
		self.date_debited = datetime.now()
		self.owner = owner
		self.owner.available_balance -= amount
		self.owner.ledger_balance -= amount

	def to_dict(self):
		payload = {
			'amount': self.amount,
			'debited_by': self.debitor_id,
			'date_debited': str(self.date_debited),
		}
		return payload


class Account(Base):
	id = db.Column(db.Integer, unique=True,
				   primary_key=True, autoincrement=True)
	group_id = db.Column(db.String, unique=True, nullable=False)
	customer_id = db.Column(db.String, unique=True, nullable=True)
	available_balance = db.Column(db.Float, default=0)
	ledger_balance = db.Column(db.Float, default=0, nullable=False)
	pending_amount = db.Column(db.Float, default=0, nullable=False)
	last_activation = db.Column(db.DateTime)
	paystack_auth = db.Column(db.String, unique=False, nullable=True)
	last_four = db.Column(db.Integer, unique=False, nullable=True)
	brand = db.Column(db.String, nullable=True)
	is_suspended = db.Column(db.Boolean, default=False, nullable=False)
	all_credits = db.relationship('Credit', backref='owner', lazy='dynamic')
	all_debits = db.relationship('Debit', backref='owner', lazy='dynamic')

	def __init__(self, group_id, customer_id=None, paystack_auth=None, last_four=None, brand=None):
		self.group_id = group_id
		self.customer_id = customer_id
		self.paystack_auth = paystack_auth
		self.last_four = last_four
		self.brand = brand

	@staticmethod
	def create_account(group_id, customer_id=None, paystack_auth=None, last_four=None, brand=None):
		new_account = Account(group_id, customer_id, paystack_auth, last_four, brand)
		db.session.add(new_account)
		db.session.commit()
		db.session.refresh(new_account)
		return new_account

	@staticmethod
	def get_account(group_id):
		user_account = Account.query.filter_by(group_id=group_id).first()
		return user_account

	def get_reference(self, reference_id):
		reference = self.all_credits.filter_by(reference_id=reference_id).first()
		return reference

	@staticmethod
	def new_payment(group_id, amount_paid, paid_at=None, reference=None, customer_id=None, tx_performed_by=None, is_working_balance=None, paystack_auth=None,
			last_four=None, meter_id=None, energy_units_purchased=None, brand=None):
		"""
			To be called when a new payment via the
			payment gateway has been made.
		"""
		owner = Account.get_account(group_id) or \
				Account.create_account(group_id=group_id, customer_id=customer_id, paystack_auth=paystack_auth,
									   last_four=last_four, brand=brand)
		expected_values = {
			'owner': owner,
			'amount_paid': amount_paid,
			'paid_at': paid_at,
			'reference_id': reference,
			'tx_performed_by': tx_performed_by,
			'is_working_balance': is_working_balance,
			'meter_id': meter_id,
			'energy_units_purchased': energy_units_purchased
		}

		logger.info(f"Owner {owner}")
		credit = Credit(**expected_values)
		db.session.add(credit)
		try:
			db.session.commit()
			db.session.refresh(owner)
			db.session.refresh(credit)
		except Exception as e:
			logger.exception(e)
			db.session.rollback()
			raise
		print('credit', credit)
		return credit.to_dict()

	@staticmethod
	def details(group_id):
		user_account = Account.get_account(group_id)
		if user_account is None:
			raise NotFoundError('Account not found')
		credit_history = user_account.all_credits.order_by(Credit.date_credited.desc()).all()
		debit_history = user_account.all_debits.order_by(Debit.date_debited.desc()).all()

		if len(credit_history) >= 1:
			json_credit = [credit.to_dict() for credit in credit_history]
		else:
			json_credit = []

		if len(debit_history) >= 1:
			json_debit = [debit.to_dict() for debit in debit_history]
		else:
			json_debit = []

		return user_account.to_dict(json_credit, json_debit)

	@staticmethod
	def credit_history(group_id):
		user_account = Account.get_account(group_id)
		if user_account is None:
			raise NotFoundError('Account not found')
		credit_history = user_account.all_credits.order_by(Credit.date_credited.desc()).all()
		if len(credit_history) >= 1:
			json_credit = [credit.to_dict() for credit in credit_history]
			return json_credit
		json_credit = []
		return json_credit

	@staticmethod
	def debit_history(group_id):
		user_account = Account.get_account(group_id)
		if user_account is None:
			raise NotFoundError('Account not found')
		debit_history = user_account.all_debits.order_by(Debit.date_debited.desc()).all()
		if len(debit_history) >= 1:
			json_debit = [debit.to_dict() for debit in debit_history]
			return json_debit
		json_debit = []
		return json_debit

	@staticmethod
	def perform_activation(user_account, activated_amount):
		# Debit any pending debts
			# the pending amount should ideally be zero, however, if the electricity disconnetion event fails the user will keep being debitted and they will have to pay outstanding debts.

		if user_account.pending_amount > activated_amount:
			# the pending amount should ideally be zero, however, if the electricity disconnetion event fails the user will keep being debitted and they will have to pay outstanding debts.
			user_account.pending_amount -= activated_amount
		else:
			available_balance = activated_amount - user_account.pending_amount
			user_account.pending_amount = 0  # no more pending amounts
			user_account.available_balance += available_balance
		user_account.last_activation = datetime.now()
		return user_account

	@staticmethod
	def activate_credit(group_id, reference):
		"""
			To be called when a user's pin is successfuly
			validated
		"""
		user_account = Account.get_account(group_id)

		if user_account is None:
			raise NotFoundError('Account not found')

		target_activation = user_account.all_credits.filter_by(
			reference_id=reference).first()

		if target_activation is None:
			raise NotFoundError('Credit Reference not found')

		if target_activation.is_activated:
			raise ActivationError('Amount already activated')
		activated_amount = target_activation.amount_paid
		user_account = Account.perform_activation(user_account, activated_amount)
		target_activation.is_activated = True

		# # Debit any pending debts
		# 	# the pending amount should ideally be zero, however, if the electricity disconnetion event fails the user will keep being debitted and they will have to pay outstanding debts.

		# if user_account.pending_amount > activated_amount:
		# 	# the pending amount should ideally be zero, however, if the electricity disconnetion event fails the user will keep being debitted and they will have to pay outstanding debts.
		# 	user_account.pending_amount -= activated_amount
		# else:
		# 	available_balance = activated_amount - user_account.pending_amount
		# 	user_account.pending_amount = 0  # no more pending amounts
		# 	user_account.available_balance += available_balance
		# user_account.last_activation = datetime.now()


		db.session.add(user_account)
		db.session.add(target_activation)
		db.session.commit()
		db.session.refresh(user_account)
		db.session.refresh(target_activation)

		resp = Account.latest_ref(group_id)
		resp['ledger_balance'] = user_account.ledger_balance
		resp['available_balance'] = user_account.available_balance
		resp['pending_amount'] = user_account.pending_amount
		resp['amount_activated'] = target_activation.is_activated
		return resp

	def ensure_activation(self, activated_reference_list):
		for reference_id in activated_reference_list:
			credit = Credit.get_reference(reference_id)
			if credit:
				if credit.is_activated:
					continue
				account = credit.owner
				activated_amount = credit.amount_paid
				user_account = Account.perform_activation(account, activated_amount)
				credit.is_activated = True
				db.session.add(user_account)
				db.session.add(credit)
		db.session.commit()
		db.session.refresh(self)
		return self.balance_summary()

	@staticmethod
	def debit_user(group_id, amount, debitor_id=None):
		amount = float(amount)
		account = Account.get_account(group_id)
		if account.is_suspended:
			# Disconnect user in the transaction engine
			raise AccountSuspendedError(
				'<User {}>\'s Account Suspended'.format(group_id))
		if (account.available_balance < amount):
			# Disconnect user in the transaction engine
			account.pending_amount += amount
			db.session.add(account)
			db.session.commit()
			db.session.refresh(account)
			raise InsufficientFundsError(
				'Insufficient funds for user {}! | Current balance = {} | Total Amount being Owed = {}'.format(
					group_id,
					account.available_balance,
					account.pending_amount,
				))
		debit = Debit(debitor_id, amount, owner=account)
		db.session.add(debit)
		db.session.add(account)
		db.session.commit()
		db.session.refresh(account)
		return account.balance_summary()

	@staticmethod
	def suspend_user(group_id, to_suspend):
		user_account = Account.get_account(group_id)
		if user_account is None:
			raise NotFoundError('Account Not Found')
		user_account.is_suspended = to_suspend
		db.session.add(user_account)
		db.session.commit()
		return Account.details(group_id)

	@staticmethod
	def latest_ref(group_id):
		user_account = Account.get_account(group_id)
		if user_account is None:
			raise NotFoundError('Account Not Found')
		reference = user_account.all_credits.order_by(desc(Credit.id)).first()
		return reference.to_dict()

	@staticmethod
	def all_accounts():
		accounts = Account.query.all()
		if len(accounts) == 0:
			return []
		response = [Account.details(account.group_id)
					for account in accounts if account.group_id]
		return response

	# def to_dict(self, credit_history, debit_history, paystack_auth, last_four, brand):
	def to_dict(self, credit_history, debit_history):
		details = {
			'group_id': self.group_id,
			'user_id': self.customer_id,
			'account_created': str(self.date_created),
			'ledger_balance': self.ledger_balance,
			'available_balance': self.available_balance,
			'pending_amount': self.pending_amount,
			'is_suspended': self.is_suspended,
			'last_activation': str(self.last_activation),
			'credit_history': credit_history,
			'debit_history': debit_history,
			'paystack_auth': self.paystack_auth,
			'last_four': self.last_four,
			'brand': self.brand,
		}
		return details

	def account_balance(self):
		return self.balance_summary()

	def balance_summary(self):
		details = {
			'group_id': self.group_id,
			'ledger_balance': self.ledger_balance,
			'available_balance': self.available_balance,
			'pending_amount': self.pending_amount,
		}
		return details
