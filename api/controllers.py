from collections import namedtuple

import arrow
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import Schema, ValidationError
from marshmallow import fields as ma_fields
from marshmallow import post_load, validates

from api import api
from api.manager import AccountManager
from logger import logger

CreditUser = namedtuple('CreditUser', [
	'paid_at',
	'group_id',
	'reference',
	'amount_paid',
	'customer_id',
	'tx_performed_by',
	'is_working_balance',
	'paystack_auth',
	'last_four',
	'brand',
	'meter_id',
	'energy_units_purchased',
])

CreditUser.__new__.__defaults__ = (None,) * len(CreditUser._fields)

BulkCreditUser = namedtuple('BulkCreditUser', [
	'bulk_credit',
])

DebitUser = namedtuple('DebitUser', [
	'group_id',
	'debitor_id',
	'amount',
])

ActivateUser = namedtuple('ActivateUser', [
	'group_id',
	'debitor_id',
	'matched',
])


ReferenceList = namedtuple('ReferenceList', [
	'reference_list',
])


class CreditSchema(Schema):
	paid_at = ma_fields.String(required=True)
	group_id = ma_fields.String(required=True)
	reference = ma_fields.String(required=True)
	amount_paid = ma_fields.Float(required=True)
	customer_id = ma_fields.String(required=True)
	tx_performed_by = ma_fields.String(required=True)
	is_working_balance = ma_fields.Boolean(required=True)
	paystack_auth = ma_fields.String(required=True)
	last_four = ma_fields.Float(required=True)
	brand = ma_fields.String(required=True)
	meter_id = ma_fields.String(required=False, allow_none=True)  # Allow users without a meter recharge their accounts.
	energy_units_purchased = ma_fields.String(required=False, allow_none=True)  # Allow users without an energy plan recharge their accounts.


	@post_load
	def activate(self, data):
		return CreditUser(**data)

	@validates('paid_at')
	def validate_paid_at(self, value):
		try:
			arrow.get(value).datetime
		except Exception:
			raise ValidationError('Time the transaction was made.')

	@validates('group_id')
	def validate_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Group ID required.')

	@validates('reference')
	def validate_reference(self, value):
		if len(value) <= 0:
			raise ValidationError('Transaction reference required.')

	@validates('amount_paid')
	def validate_amount_paid(self, value):
		try:
			int(value)
		except Exception:
			raise ValidationError('Amount paid required.')


	@validates('customer_id')
	def validate_customer_id(self, value):
		if len(value) <= 0:
			raise ValidationError('ID of the user who will be credited is required.')

	@validates('tx_performed_by')
	def validate_tx_performed_by(self, value):
		if len(value) <= 0:
			raise ValidationError('Transaction performer required.')

	@validates('is_working_balance')
	def validate_is_working_balance(self, value):
		if not isinstance(value, bool):
			raise ValidationError('Need to know if to credit working balance or not.')

	@validates('paystack_auth')
	def validate_paystack_auth(self, value):
		if not value:
			raise ValidationError('Authorization code is required from paystack')

	@validates('last_four')
	def validate_last_four(self, value):
		if not value:
			raise ValidationError('last four degits of Card must be known')

	@validates('brand')
	def validate_brand(self, value):
		if not value:
			raise ValidationError('Card brand required')

	def __repr__(self):
		return "Credit ><{}".format(self.group_id)


class BulkCreditSchema(Schema):
	bulk_credit = ma_fields.Nested('CreditSchema', many=True, strict=True)

	@post_load
	def activate(self, data):
		return BulkCreditUser(**data)


class DebitSchema(Schema):
	group_id = ma_fields.String(required=True)
	debitor_id = ma_fields.String(required=True)
	amount = ma_fields.Float(required=True)

	@post_load
	def activate(self, data):
		return DebitUser(**data)

	@validates('group_id')
	def validate_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	@validates('debitor_id')
	def validate_debitor_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	@validates('amount')
	def validate_amount(self, value):
		try:
			int(value)
		except Exception:
			raise ValidationError('Value must be a numerical object')




	def __repr__(self):
		return "Debit <{}>".format(self.group_id)


class ActivationSchema(Schema):
	group_id = ma_fields.String(required=True)
	reference = ma_fields.String(required=True)
	matched = ma_fields.Boolean(required=True)

	@post_load
	def activate(self, data):
		return ActivateUser(**data)

	@validates('group_id')
	def validate_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	@validates('reference')
	def validate_reference(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	@validates('matched')
	def validate_matched(self, value):
		if not isinstance(value, bool):
			raise ValidationError('Value cannot be Boolean')

	def __repr__(self):
		return "Activate <{}>".format(self.group_id)


class EnsureActivationSchema(Schema):
	reference_list = ma_fields.List(ma_fields.String, required=True)

	@post_load
	def activate(self, data):
		return ReferenceList(**data)

	def __repr__(self):
		return "<Ensure activation>"


account_api = Namespace('accounts', description='Api for managing user accounts')

# For swagger documentation
credit = account_api.model('credit', {
	'paid_at': fields.String(required=True, description='The time the transaction was performed'),
	'group_id': fields.String(required=True, description='The user\'s group ID'),
	'reference': fields.String(required=True, description='The reference ID of the transaction.'),
	'amount_paid': fields.String(required=True, description='The amount the to be credited'),
	'customer_id': fields.String(required=True, description='The ID of the user to be credited.'),
	'tx_performed_by': fields.String(required=True, description='The ID of the user performing the credit'),
	'is_working_balance': fields.Boolean(required=True, description='Info required to determine if also credit the main account balance'),
	'paystack_auth': fields.String(required=True, description='The authorization code from paystack'),
	'last_four': fields.Integer(required=True, description='The last four digits of the card'),
	'brand': fields.String(required=True, description='The brand of the card'),
	'meter_id': fields.String(required=False, description='The ID of the meter the purchase is meant for'),
	'energy_units_purchased': fields.String(required=False, description='The energy units purchased.'),
})

blockchain_credit = account_api.model('blockchain_credit', {
	'group_id': fields.String(required=True, description='The user\'s group ID'),
	'amount_paid': fields.String(required=True, description='The amount the to be credited'),
	'reference': fields.String(required=True, description='Reference of the transaction'),
	'is_working_balance': fields.Boolean(required=True, description="Info required to determine if also credit the main account balance"),
})



bulk_credit = account_api.model('bulk credit', {
	'bulk_credit': fields.List(fields.Nested(credit, required=True, description='List of credits to be performed.')),
})

debit = account_api.model('debit', {
	'group_id': fields.String(required=True, description='The user\'s group ID'),
	'debitor_id': fields.String(required=True, description='The ID of the user performing the debit'),
	'amount': fields.String(required=True, description='The amount to be debited'),
})

blockchain_debit = account_api.model('blockchain_debit', {
	'group_id': fields.String(required=True, description='The user\'s group ID'),
	'amount': fields.String(required=True, description='The amount to be debited'),
})


activate = account_api.model('activate', {
	'group_id': fields.String(required=True, description='The user\'s group ID'),
	'reference': fields.String(required=True, description='The reference ID of the amount to be activated'),
	'matched': fields.Boolean(required=True, description='Is the pin correct?'),
})

ensure_activation = account_api.model('ensure_activation', {
	'reference_list': fields.List(fields.String, required=True, description='List of references'),
})

@account_api.route('/credit/')
class Transactions(Resource):
	"""
		Api to credit a user's account
	"""

	# @account_api.expect(credit)  # For swagger documentation
	def post(self):
		"""
			HTTP method for making account credits
			@param: request payload
			@returns: response and status code
		"""
		# schema = CreditSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		logger.info("data received from paystack ....")
		logger.info(payload)

		response = {}
		#
		# try:
		# 	credit_payload = schema.load(payload).data._asdict()
		# except ValidationError as e:
		# 	logger.exception(e.messages)
		# 	response['success'] = False
		# 	response['message'] = e.messages
		# 	return response, 400

		manager = AccountManager()
		resp, code = manager.new_payment(payload)
		return resp, code

@account_api.route('/blockchain-credit/')
class BlockchainCredit(Resource):
	"""
		Api to credit a user's account
	"""

	@account_api.expect(blockchain_credit)
	def post(self):
		"""
			HTTP method for making account credits
			@param: request payload
			@returns: response and status code
		"""
		schema = CreditSchema()
		data = request.values.to_dict()
		payload = api.payload or data

		response = {}

		try:
			credit_payload = schema.load(payload, partial=('group_id', 'amount_paid', 'reference', 'is_working_balance')).data
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['message'] = e.messages
			return response, 400

		manager = AccountManager()
		resp, code = manager.new_payment(credit_payload)
		return resp, code


@account_api.route('/bulk-credit/')
class BulkCredit(Resource):
	"""
		Api to credit a user's account
	"""

	@account_api.expect(bulk_credit)  # For swagger documentation
	def post(self):
		"""
			HTTP method for making account credits
			@param: request payload
			@returns: response and status code
		"""
		from flask import current_app
		if current_app.config.get('FLASK_ENV') in {'prod', 'production', 'Production'}:
			response = dict(message='Unauthorized')
			return response, 403
		schema = BulkCreditSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data

		response = {}

		try:
			credit_payload = schema.load(payload).data._asdict()
			credit_payload['bulk_credit'] = [dict(credit._asdict()) for credit in credit_payload['bulk_credit']]
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['message'] = e.messages
			return response, 400

		manager = AccountManager()
		resp, code = manager.new_bulk_payment(credit_payload)
		return resp, code


@account_api.route('/activate/')
class Activate(Resource):
	"""
		Api to activate a user's account
	"""

	@account_api.expect(activate)  # For swagger documentation
	def put(self):
		"""
			HTTP method for activating an account credit.
			@param: group_id
			@returns: response and status code
		"""
		schema = ActivationSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data

		try:
			activation_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = e.messages
			return response, 400

		manager = AccountManager()
		resp, code = manager.activate_credit(activation_payload)
		return resp, code


@account_api.route('/ensure-activation/')
class EnsureActivation(Resource):
	"""
		Api to activate a user's account
	"""

	@account_api.expect(ensure_activation)  # For swagger documentation
	def post(self):
		"""
			HTTP method for ensuring activated pins are reflected in a user's account.
			@returns: response and status code
		"""
		schema = EnsureActivationSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data

		try:
			activation_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = e.messages
			return response, 400

		manager = AccountManager()
		
		group_id = request.cookies.get('group_id')
		resp, code = manager.ensure_activation(group_id, activation_payload)
		return resp, code


@account_api.route('/all-accounts/')
class AllAccounts(Resource):
	"""
		Api to view all accounts
	"""

	def get(self):
		"""
			HTTP method for viewing the account details of all users
			@param: request payload
			@returns: response and status code
		"""
		manager = AccountManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_all_accounts(role_id)
		return resp, code


@account_api.route('/view-tx/<string:group_id>/<string:reference_id>/')
class OneUserTranaction(Resource):
	"""
		Api for user credit
	"""

	def get(self, group_id, reference_id):
		"""
			ADMIN method for viewing a user's account details
			@param: group_id
			@returns: response and status code
		"""
		manager = AccountManager()
		requester_role = request.cookies.get('role_id')
		resp, code = manager.view_transaction(requester_role, group_id, reference_id)
		return resp, code


@account_api.route('/tx-engine/<string:group_id>/')
class TxEngineAccount(Resource):
	"""
		Api for user credit
	"""

	def get(self, group_id):
		"""
			ADMIN method for viewing a user's account details
			@param: group_id
			@returns: response and status code
		"""
		manager = AccountManager()
		resp, code = manager.view_tx_account(group_id)
		return resp, code


@account_api.route('/my-account-balance/')
class MyAccountBalance(Resource):
	"""
		Api for a user to see self's debit history
	"""

	def get(self):
		"""
			HTTP method for a user to her account balance
			@param:
			@returns: response and status code
		"""
		manager = AccountManager()
		group_id = request.cookies.get('group_id')
		resp, code = manager.account_balance(group_id)
		return resp, code


@account_api.route('/one/<string:group_id>/')
class UserAccountInfo(Resource):
	"""
		Api for a user to see his account details.
	"""

	def get(self, group_id):
		"""
			HTTP method for an user to his account details
			@param: group_id user's group ID
			@returns: response and status code
		"""
		manager = AccountManager()
		viewer_role_id = request.cookies.get('role_id')
		viewer_group_id = request.cookies.get('group_id')
		resp, code = manager.user_account(viewer_role_id, viewer_group_id, group_id)
		return resp, code


@account_api.route('/my-account/')
class MyAccount(Resource):
	"""
		Api for user credit
	"""

	def get(self):
		"""
			HTTP method for a user to see his account details
			@param:
			@returns: response and status code
		"""
		manager = AccountManager()
		group_id = request.cookies.get('group_id')
		resp, code = manager.one_account(group_id)
		return resp, code


@account_api.route('/debit/')
class Debit(Resource):
	"""
		Api for user credit
	"""
	@account_api.expect(debit)  # For swagger documentation
	def post(self):
		"""
			HTTP method for account debits
			@param: request payload
			@returns: response and status code
		"""
		schema = DebitSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data

		try:
			debit_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
			# logger.exception(e)
		manager = AccountManager()
		resp, code = manager.debit_user(debit_payload)
		return resp, code

@account_api.route('/blockchain-debit/')
class BlockchainDebit(Resource):
	"""
		Api for user credit
	"""
	@account_api.expect(blockchain_debit)  # For swagger documentation
	def post(self):
		"""
			HTTP method for account debits
			@param: request payload
			@returns: response and status code
		"""
		schema = DebitSchema()
		data = request.values.to_dict()
		payload = api.payload or data

		try:
			debit_payload = schema.load(payload, partial=('amount', 'group_id')).data
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = AccountManager()
		resp, code = manager.debit_user(debit_payload)
		return resp, code


@account_api.route('/credit-history/<string:group_id>/')
class UserCreditHistory(Resource):
	"""
		Api for user credit
	"""

	def get(self, group_id):
		"""
			ADMIN method for viewing a user's credit history
			@param: group_id
			@returns: response and status code
		"""
		manager = AccountManager()
		requester_role = request.cookies.get('role_id')
		resp, code = manager.view_credit_history(requester_role, group_id)
		return resp, code


@account_api.route('/debit-history/<string:group_id>/')
class UserDebitHistory(Resource):
	"""
		Api for user credit
	"""

	def get(self, group_id):
		"""
			ADMIN method for viewing a user's credit history
			@param: group_id
			@returns: response and status code
		"""
		manager = AccountManager()
		requester_role = request.cookies.get('role_id')
		resp, code = manager.view_debit_history(requester_role, group_id)
		return resp, code


@account_api.route('/my-credit-history/')
class MyCreditHistory(Resource):
	"""
		Api for a user to see self's credit history
	"""

	def get(self):
		"""
			HTTP method for a user to see self's credit history
			@param:
			@returns: response and status code
		"""
		manager = AccountManager()
		group_id = request.cookies.get('group_id')
		resp, code = manager.my_credit_history(group_id)
		return resp, code


@account_api.route('/my-debit-history/')
class MyDebitHistory(Resource):
	"""
		Api for a user to see self's debit history
	"""

	def get(self):
		"""
			HTTP method for a user to see self's debit history
			@param:
			@returns: response and status code
		"""
		manager = AccountManager()
		group_id = request.cookies.get('group_id')
		resp, code = manager.my_debit_history(group_id)
		return resp, code
