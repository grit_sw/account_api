class ActivationError(ValueError):
    """docstring for ActivationError"""
    pass


class NotFoundError(AttributeError):
    """Inheriting from AttributError since sqlalchemy returns None if a user is not found"""
    pass


class AccountSuspendedError(ValueError):
    """docstring for AccountSuspendedError"""
    pass


class InsufficientFundsError(ValueError):
    """docstring for InsufficientFundsError"""
    pass
