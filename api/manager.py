from api.models import Account
from logger import logger
from sqlalchemy.exc import IntegrityError, DataError
from api.exceptions import NotFoundError, AccountSuspendedError, InsufficientFundsError, ActivationError


class AccountManager(object):
	"""
		Class to manage user creation and retrieval
	"""

	def new_payment(self, payload):
		"""
			Method to perform account legder credit, called after payment
			@args: data payload
			@returns: a status message and status code
		"""
		response = {}
		event = payload['event']
		if event == "charge.success":
			amount_paid = float(payload['data']['amount'])
			paid_at = payload['data']['paid_at']
			reference = payload['data']['reference']

			# brand = payload['data']['authorization']['brand']
			# last_four = payload['data']['authorization']['last4']
			paystack_auth = payload['data']['authorization']['authorization_code']
			if not payload['data']['metadata']:
				logger.info('\n\n\n\n\n')
				logger.info('Invalid message format')
				logger.info('\n\n\n\n\n')
				response['success'] = False
				response['message'] = 'Invalid metadata sent'
				return response, 200  # invalid message format. Send a success message so paystack doesn't keep resending it.

			group_id = payload['data']['metadata']['group_id']
			# tx_performed_by = payload['data']['metadata']['tx_performed_by']
			meter_id = payload['data']['metadata'].get('meter_id')
			energy_units_purchased = payload['data']['metadata'].get('energy_units_purchased')

			account_data = {
				'amount_paid': amount_paid,
				'energy_units_purchased': energy_units_purchased,
				'paystack_auth': paystack_auth,
				'meter_id': meter_id,
				'paid_at': paid_at,
				'group_id': group_id,

			}
			Account.new_payment(**account_data)
			response['success'] = True
			return response, 200

		response['success'] = False
		return response, 400

	def new_bulk_payment(self, payload):
		"""
			Method to perform account legder credit, called after payment
			@args: data payload
			@returns: a status message and status code
		"""
		response = {}
		output = []
		duplicates = []
		# paystack_auth= payload['paystack_auth']
		for credit in payload['bulk_credit']:
			group_id = credit['group_id']
			owner = Account.get_account(group_id)
			if owner:
				reference = credit['reference']
				duplicate_tx = owner.get_reference(reference)
				if duplicate_tx:
					duplicates.append(duplicate_tx.to_dict())

			try:
				response_data = Account.new_payment(**credit)
				output.append(response_data)

			except IntegrityError as e:
				logger.critical(e)
				response['success'] = False
				response['message'] = 'Database Error'
				return response, 500

			except DataError as e:
				logger.critical(e)
				response['success'] = False
				response['message'] = 'Database Error'
				return response, 500

			except Exception as e:
				logger.critical(e)
				response['success'] = False
				response['message'] = 'Database Error'
				return response, 500

		response['success'] = True
		response['data'] = output
		response['duplicates'] = duplicates
		return response, 201

	def activate_credit(self, payload):
		"""
			Method to view one user's account details
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		matched = payload['matched']
		if not matched:
			logger.info("Pin Not Checked")
			response['success'] = False
			response['message'] = "Pin not checked"
			return response, 403
		del payload['matched']

		try:
			response_data = Account.activate_credit(**payload)
			response['success'] = True
			response['data'] = response_data
			return response, 201

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = e
			return response, 404

		except ActivationError as e:
			logger.info(e)
			response['success'] = False
			response['message'] = 'Amount already activated'
			return response, 409

	def debit_user(self, debit_payload):
		"""
			Method to perform account debit
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		debit_params = debit_payload
		group_id = debit_params['group_id']
		amount = debit_params['amount']

		account = Account.get_account(group_id) or Account.create_account(group_id=group_id)
		try:
			debited_account = account.debit_user(**debit_params)
			response['success'] = True
			response['data'] = debited_account
			logger.info('\n')
			logger.info('User {} debited {}'.format(group_id, amount))
			return response, 201

		except AccountSuspendedError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Account Suspended'
			return response, 403

		except InsufficientFundsError as e:
			logger.info('\n')
			logger.info(e)
			response['success'] = False
			response['message'] = 'Insufficient funds'
			response['data'] = account.balance_summary()
			return response, 403

	def view_all_accounts(self, requester_role):
		"""
			Method to view all users account details
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		if int(requester_role) != 5:
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		try:
			response_data = Account.all_accounts()
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Database Error'
			return response, 400

	def view_tx_account(self, group_id):
		"""
			Transaction engine method to view one user's account details
			@args: the account's group_id
			@returns: a response message and status code
		"""
		response = {}
		if not isinstance(group_id, str):
			response['success'] = False
			response['message'] = "Bad Message"
			return response, 400

		try:
			response_data = Account.account_balance(group_id)
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Account not found'
			return response, 404

	def view_transaction(self, requester_role, group_id, reference_id):
		"""
			Transaction engine method to view one user's account details
			@args: the account's group_id
			@returns: a response message and status code
		"""
		response = {}
		if not requester_role:
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		if int(requester_role) != 5:
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		account = Account.get_account(group_id)
		if not account:
			response['success'] = False
			response['message'] = 'Account or reference ID not found.'
			return response, 404

		transaction = account.get_reference(reference_id)
		if not transaction:
			response['success'] = False
			response['message'] = 'Account or reference ID not found.'
			return response, 404

		response['success'] = True
		response['data'] = transaction.to_dict()
		return response, 201

	def view_credit_history(self, requester_role, group_id):
		"""
			Method to view one user's credit history
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		if int(requester_role) != 5:
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		if group_id is None:
			response['success'] = False
			response['message'] = "Bad Message"
			return response, 400

		try:
			response_data = Account.credit_history(group_id)
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Account not found'
			return response, 404

	def view_debit_history(self, requester_role, group_id):
		"""
			Method to view one user's debit history
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		if int(requester_role) != 5:
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		if group_id is None:
			response['success'] = False
			response['message'] = "Bad Message"
			return response, 400

		try:
			response_data = Account.debit_history(group_id)
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Account not found'
			return response, 404

	def my_credit_history(self, group_id):
		"""
			Method to view one user's debit history
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		if group_id is None:
			response['success'] = False
			response['message'] = "Bad Message"
			return response, 400

		try:
			response_data = Account.credit_history(group_id)
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Account not found'
			return response, 404

	def my_debit_history(self, group_id):
		"""
			Method to view one user's debit history
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		if group_id is None:
			response['success'] = False
			response['message'] = "Bad Message"
			return response, 400

		try:
			response_data = Account.debit_history(group_id)
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Account not found'
			return response, 404

	def account_balance(self, group_id):
		"""
			Method to view one user's account balance
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		account = Account.get_account(group_id)
		if not account:
			response['success'] = False
			response['message'] = "Account not found."
			return response, 404

		response_data = account.account_balance()
		response['success'] = True
		response['data'] = response_data
		return response, 200

	def one_account(self, group_id):
		"""
			Method to view one user's account details
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		if group_id is None:
			response['success'] = False
			response['message'] = "Bad Message"
			return response, 400

		try:
			response_data = Account.details(group_id)
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Account not found'
			return response, 404

	def user_account(self, viewer_role_id, viewer_group_id, group_id):
		"""
			Method to view one user's account details
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		if group_id is None:
			response['success'] = False
			response['message'] = "Bad Message"
			return response, 400

		if viewer_group_id != group_id:
			if viewer_role_id is None:
				response['success'] = False
				response['message'] = 'Invalid request.'
				return response, 403
			try:
				viewer_role_id_int = int(viewer_role_id)
			except ValueError:
				response['success'] = False
				response['message'] = 'Invalid request.'
				return response, 403

			if int(viewer_role_id_int) < 3:
				response['success'] = False
				response['message'] = 'Invalid request.'
				return response, 403

		try:
			response_data = Account.details(group_id)
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Account not found'
			return response, 404

		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Error when retrieving account information. Try again later.'
			return response, 400

	def ensure_activation(self, group_id, reference_payload):
		reference_list = reference_payload['reference_list']
		response = {}
		account = Account.get_account(group_id)
		if not account:
			response['success'] = False
			response['message'] = "Account not found."
			return response, 404
		try:
			account_summary = account.ensure_activation(reference_list)
		except Exception as e:
			logger.exception(e)
			response['success'] = False
			return response, 403
		response['success'] = True
		response['data'] = account_summary
		return response, 200
